#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/wait.h>
#include "signal.h"
#include <pwd.h>
#include "fonction.h"
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "path.h"
#include "redirection.h"
#include <dirent.h>

void InviteDeCommande()
{
  int toucheClavier = 0;
  char buffer[1000];
  int i = 0;

  while(1)
    {
      printf("%s",RepertoireCourant());
      i = 0;

      memset(buffer, 0, 1000 * sizeof(char));
      while( (toucheClavier = getchar()) != EOF && toucheClavier != '\n')
	{
	  if(toucheClavier == '\\')
	    {
	      printf("\n>");
	      while( (toucheClavier = getchar()) != EOF && toucheClavier != '\n')
		{
		  buffer[i] = toucheClavier;
		  i++;
		}
	    }
	  else
	    {
	      buffer[i] = toucheClavier;
	      i++;
	    }
	}

      buffer[i] = '\0';

      AnalyseCommande(buffer);

      if(toucheClavier == EOF)
	{
	  printf("\nVous avez quitté le programme\n");
	  exit(EXIT_SUCCESS);
	  break;
	}

    }
}

char** Parse(char* commande)
{
  char** arguments = (char**)malloc(100 * sizeof(char*));
  char* chaineTampon = (char*)malloc(256 * sizeof(char));
  int i = 0; /* indice de debut de chaine de caractere */
  int j = 0; /* indice de rencontre espace */
  int k = 0; /* indice de chaineTampon */
  int l = 0; /* indice de arguments */
  int taille = strlen(commande);

  while(i < taille)
    {
      if(commande[j] == '"')
	{
	  j++;
	  i++;
	  while(commande[j] != '"' && j < taille)
	    {
	      j++;
	    }
	}
      else
	{
	  while(commande[j] != ' ' && commande[j] != '|' && j < taille)
	    {
	      j++;
	    }
	}

      while(i < j)
	{
	  chaineTampon[k] = commande[i];
	  i++;
	  k++;
	}
      chaineTampon[k] = '\0';

      if(chaineTampon[0] != '\0')
	{
	  arguments[l] = (char*)malloc(256 * sizeof(char));
	  strcpy(arguments[l], chaineTampon);
	  l++;
	}

      memset(chaineTampon, 0, sizeof(256));
      i = j+1;
      j = i;
      k = 0;
    }
  arguments[l] = NULL;

  return arguments;
}

bool isSetEnv(char* cmd)
{
  char** arguments = (char**)malloc(10 * sizeof(char*));
  arguments = Parse(cmd);
  if(arguments[1] != NULL)
    {
      if(strcmp("=", arguments[1]) == 0)
	{
	  return true;
	}
    }
  return false;
}

void multi_pipe(char ***cmd)
{
  int   p[2];
  pid_t pid;
  int   fd_in = 0;

  while (*cmd != NULL)
    {
      pipe(p);
      if ((pid = fork()) == -1)
        {
          exit(EXIT_FAILURE);
        }
      else if (pid == 0)
        {
          dup2(fd_in, 0);
          if (*(cmd + 1) != NULL)
            dup2(p[1], 1);
          close(p[0]);
          execvp((*cmd)[0], *cmd);
          exit(EXIT_FAILURE);
        }
      else
        {
          wait(NULL);
          close(p[1]);
          fd_in = p[0];
          cmd++;
        }
    }
}

bool isMultiPipe(char* cmd)
{
  int nbPipe = countPipe(cmd);
  if(nbPipe > 1)
    {
      return true;
    }
  return false;
}


int countPipe(char* cmd)
{
  int i = 0;
  int nbPipe = 0;
  int taille = strlen(cmd);
  while(i < taille)
    {
      if(cmd[i] == '|')
	{
	  nbPipe++;
	}
      i++;
    }
  return nbPipe;
}

void cd(char* chemin)
{
  char path[1000];
  char* chainePath = (char*)malloc(1000 * sizeof(char));
  strcpy(path,chemin);
  chainePath = Path();
  if(chainePath[0] != '/')
    {
      strcat(chainePath, "/");
      strcat(chainePath, chemin);

      chdir(chainePath);
    }
  else
    {
      chdir(chemin);
    }

}

void AnalyseCommande(char* buffer)
{
  int redirection = estRedirection(buffer);
  char** argument;
  bool flag = false;

  if(redirection >= 0) /* il y a une redirection */
    {
      if(redirection == 0)
	{
	  redirectionEntree(buffer);
	}
      else if(redirection == 1)
	{
	  redirectionSort(buffer);
	}
      else if(redirection == 2)
	{
	  redirectionErreur(buffer);
	}
    }
  else /* il n'y a pas de redirection */
    {
      if(isPipe(buffer)) /*il y a un pipe */
	{
	  Pipe(buffer);
	}
      if(isMultiPipe(buffer))
	{
	  multi_pipe(ParsePipe(buffer));
	}
      else /* il n'y a pas de pipe */
	{
	  /* verification cd ou exit ou normale */
	  argument = Parse(buffer);

	  if(argument[0] != NULL)
	    {
	      if( strcmp("cd", argument[0]) == 0)
		{
		  flag = true;
		  cd(argument[1]);
		}
	      if( strcmp("exit", argument[0]) == 0)
		{
		  flag = true;
		  exit(1);
		}
	      if( strcmp("cat", argument[0]) == 0)
		{
		  flag = true;
		  cat(argument[1]);
		}
	      if( strcmp("ls", argument[0]) == 0)
		{
		  if(argument[1] != NULL)
		    {
		      if(strcmp("-l", argument[1]) == 0)
			{
			  flag = true;
			  ls_l(".");
			}
		    }
		}
	      if( strcmp("find", argument[0]) == 0)
		{
		  if(argument[1] != NULL)
		    {
		      flag = true;
		      find(argument[1]);
		    }
		}
	      if(isSetEnv(buffer))
		{
		  if(argument[2] != NULL)
		    {
		      modiferEnv(argument[0], argument[2]);
		    }
		}
	    }
	  if(flag == false)
	    {
	      Executer(buffer);
	    }
	  flag = false;
	}
    }
}

void modiferEnv(char* varEnv, char* nouvelleValeur)
{
  setenv(varEnv, nouvelleValeur, 1);
}

void Executer(char* commande)
{
  pid_t pid;
  char** argument = Parse(commande);

  pid = fork(); /* creation du processus fils */

  if(pid == 0) /* nous sommes dans le processus fils */
    {
      if(argument[0] != NULL)
	{
	  execvp(argument[0], argument);
	}
    }
  else /* nous sommes dans le processus pere */
    {
      wait(NULL);
    }
}


void cat(const char *fname)
{
  int fd;
  char buf[4096];
  ssize_t nread;
  if ((fd = open(fname, O_RDONLY)) == -1){
    printf("%s n'existe pas \n", fname);
    return;
  }
  //exit(EXIT_FAILURE);
   
  while ((nread = read(fd, buf, sizeof buf)) > 0) {
    ssize_t ntotalwritten = 0;
    while (ntotalwritten < nread)
      {
        ssize_t nwritten = write(STDOUT_FILENO, buf + ntotalwritten, nread - ntotalwritten);
        ntotalwritten += nwritten;
      }
  }
}

void find(char *nomFichier)
{
  char dossierCourant[100];
  struct dirent *file;
  DIR *directory = opendir(".");

  while((file = readdir(directory))!=NULL)
    {
      if(strcmp(file->d_name,"..") && strcmp(file->d_name,"."))
        {
	  if(file->d_type == DT_DIR)
            {
	      chdir(file->d_name);
	      find(nomFichier);
	      chdir("..");
            }
	  else
            {
              if(strcmp(nomFichier,file->d_name) == 0 )
		{
		  getcwd(dossierCourant,100);
		  printf("%s/%s\n", dossierCourant, file->d_name);
		}
            }
        }
    }
}
