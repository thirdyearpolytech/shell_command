#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/wait.h>
#include "signal.h"
#include <pwd.h>
#include "fonction.h"
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "path.h"
#include <dirent.h>

bool isPipe(char* buffer) {
	int i = 0;

	while (buffer[i] != '\0' && i < 1000) {
		if (buffer[i] == '|')
			return true;

		i++;
	}
	return false;
}

char* beforePipe(char* buffer) {
	int i = 0;
	int tailleBuffer = strlen(buffer);
	char* firstCmd = (char*) malloc(tailleBuffer * sizeof(char));
	while (buffer[i] != '\n' && i < tailleBuffer) {
		if (buffer[i] != '|') {
			firstCmd[i] = buffer[i];
		} else {
			break;
		}

		i++;
	}
	firstCmd[i] = '\0';

	return firstCmd;
}

char* afterPipe(char* buffer) {
	int i = 0, j = 0;
	int tailleBuffer = strlen(buffer);
	char* secondCmd = (char*) malloc(tailleBuffer * sizeof(char));

	while (buffer[i] != '\n' && i < tailleBuffer) {
		if (buffer[i] == '|') {
			i++;
			while (buffer[i] != '\n' && i < tailleBuffer) {
				while (buffer[i] == ' ' && i < tailleBuffer) {
					i++;
				}

				secondCmd[j] = buffer[i];
				i++;
				j++;
			}
		}
		i++;
	}
	secondCmd[j] = '\0';

	return secondCmd;
}

char*** ParsePipe(char* cmd) {
	int i = 0;
	int j = 0;
	int tailleString = 0;
	int tailleCmd = strlen(cmd);
	int indCmd = 0;
	int buffer = 0;
	char newCmd[256];
	int nbCmd = countPipe(cmd) + 1;
	char*** tabCmd = (char***) malloc(nbCmd * sizeof(char**));
	char* string = (char*) malloc(256 * sizeof(char));

	while (i < tailleCmd) {
		newCmd[i] = cmd[i];
		i++;
	}
	i = 0;
	while (i < nbCmd) {
		string = beforePipe(newCmd);
		tailleString = strlen(string);
		tabCmd[i] = Parse(string);
		indCmd = indCmd + tailleString + 2;
		buffer = indCmd;
		while (buffer < tailleCmd) {
			newCmd[j] = cmd[buffer];
			buffer++;
			j++;
		}
		newCmd[j] = '\0';
		j = 0;
		i++;
	}
	tabCmd[i] = NULL;
	return tabCmd;
}

void Pipe(char* buffer) {
	int pipefd[2];
	pid_t pid;
	pid_t pid1;
	char* firstCmd = beforePipe(buffer);
	char** arg1 = Parse(firstCmd);

	char* secondCmd = afterPipe(buffer);
	char** arg2 = Parse(secondCmd);

	int status = pipe(pipefd);
	if (status < 0) {
		perror("pipe");
		exit(EXIT_FAILURE);
	}

	pid1 = fork();
	if (pid1 == 0) {
		close(pipefd[0]);
		dup2(pipefd[1], 1);
		if (arg1[0] != NULL) {
			execvp(arg1[0], arg1);
		}
		exit(EXIT_SUCCESS);
	}
	pid = fork();
	if (pid == 0) {
		wait(NULL);
		close(pipefd[1]);
		dup2(pipefd[0], 0);
		if (arg2[0] != NULL) {
			execvp(arg2[0], arg2);
		}
		exit(EXIT_SUCCESS);
	}
	close(pipefd[0]);
	close(pipefd[1]);
	wait(NULL);
}

char* detect_nom_fichier(char* buffer) {
	int i = 0; /* indice de parcours avant de detecter les '>' et les '<'*/
	int k = 0; /* pour indiquer la fin de la chaine renvoyee*/
	int taill_buff = 0;

	taill_buff = strlen(buffer);
	char* nom_fichier = (char*) malloc(taill_buff * sizeof(char));

	while (buffer[i] != '\n' && i < taill_buff) {

		if ((buffer[i] == '>') || (buffer[i] == '<')) {
			i++;
			while (buffer[i] != '\n' && i < taill_buff) {
				while (buffer[i] == ' ' && i < taill_buff) {
					i++;
				}
				nom_fichier[k] = buffer[i];
				i++;
				k++;
			}
		}
		if (buffer[i] == '2') {
			i++;
			if (buffer[i] == '>') {
				i++;
				while (buffer[i] != '\n' && i < taill_buff) {
					while (buffer[i] == ' ' && i < taill_buff) {
						i++;
					}

					nom_fichier[k] = buffer[i];
					i++;
					k++;
				}
			}

		}
		i++;
	}
	nom_fichier[k] = '\0';
	return nom_fichier;
}

void redirectionSort(char* buffer) {
	int fdOut, retOut, stdOut;
	int i = 0;
	char buffer2[1000];

	char* mon_fichier = detect_nom_fichier(buffer);

	fdOut = open(mon_fichier, O_CREAT | O_APPEND | O_WRONLY, 0666);

	if (fdOut < 0) {
		perror("Ouverture du fichier");
		exit(1);
	}

	stdOut = dup(1);
	retOut = dup2(fdOut, 1); /* redirection de la sortie standard indiqué par le 2e parametre */

	if (retOut < 0) {
		perror("dup2 fdOut");
	}

	while (buffer[i] != '>' && i < 1000) {
		buffer2[i] = buffer[i];
		i++;
	}
	buffer2[i] = '\0';

	Executer(buffer2);
	fflush(stdout);
	close(fdOut);

	dup2(stdOut, 1);
}

void redirectionEntree(char* buffer) {
	int fdIn, retIn, stdIn;
	int i = 0;

	char buffer2[1000];

	char* mon_fichier = detect_nom_fichier(buffer);

	fdIn = open(mon_fichier, O_RDONLY, 0666);

	if (fdIn < 0) {
		perror("Ouverture du fichier");
		exit(1);
	}

	stdIn = dup(0);
	retIn = dup2(fdIn, 0); /* redirection de la sortie standard indiqué par le 2e parametre */

	if (retIn < 0) {
		perror("dup2 fdIn");
	}

	while (buffer[i] != '<' && i < 1000) {
		buffer2[i] = buffer[i];
		i++;
	}
	buffer2[i] = '\0';

	Executer(buffer2);
	fflush(stdin);
	close(fdIn);
	dup2(stdIn, 0);

}

void redirectionErreur(char* buffer) {
	int fdErr, retErr, stdErr;
	int i = 0;
	char** argument;
	char buffer2[1000];

	argument = Parse(buffer);

	char* mon_fichier = detect_nom_fichier(buffer);

	fdErr = open(mon_fichier, O_CREAT | O_APPEND | O_WRONLY, 0666);

	if (fdErr < 0) {
		perror("Ouverture du fichier");
		exit(1);
	}

	stdErr = dup(2);
	retErr = dup2(fdErr, 2); /* redirection de la sortie standard indiqué par le 2e parametre */

	if (retErr < 0) {
		perror("dup2 fdErr");
	}

	while (buffer[i] != '2' && i < 1000) {
		buffer2[i] = buffer[i];
		i++;
	}
	buffer2[i] = '\0';

	if (argument[0] != NULL) {
		if (strcmp("cd", argument[0]) == 0) {
			cd(argument[1]);
		} else if (strcmp("exit", argument[0]) == 0) {
			exit(1);
		} else {
			Executer(buffer2);
		}

	}

	fflush(stderr);
	close(fdErr);
	dup2(stdErr, 2);
}

int estRedirection(char* buffer) {
	int i = 0;

	while (buffer[i] != '\0' && i < 1000) {
		if (buffer[i] == '<') {
			return 0;
		}
		if (buffer[i] == '>') {
			return 1;
		}
		if (buffer[i] == '2') {
			if (buffer[i + 1] == '>') {

				return 2;
			}
		}

		i++;
	}
	return -1;
}

void ls_l(char* dir_name) {
	DIR *mydir;
	struct dirent *myfile;
	struct stat mystat;
	char buf[512];
	mydir = opendir(dir_name);

	while ((myfile = readdir(mydir)) != NULL) {
		sprintf(buf, "%s/%s", dir_name, myfile->d_name);
		stat(buf, &mystat);
		printf("%ld", mystat.st_size);
		printf(" %s\n", myfile->d_name);
	}

	closedir(mydir);
}
