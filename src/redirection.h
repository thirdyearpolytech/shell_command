
#ifndef REDIRECTION_H
#define REDIRECTION_H

#include <stdbool.h>

int estRedirection(char* buffer);
void redirectionSort(char* buffer);
void redirectionEntree(char* buffer);
void redirectionErreur(char* buffer);
char* detect_nom_fichier(char* buffer);
bool isPipe(char* buffer);
void Pipe(char* buffer);
char* beforePipe(char* buffer);
char* afterPipe(char* buffer);
char*** ParsePipe(char* cmd);
void ls_l(char* dir_name);

#endif
