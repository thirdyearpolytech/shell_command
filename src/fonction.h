#ifndef FONCTION_H
#define FONCTION_H

#include <stdbool.h>

void InviteDeCommande();
char** Parse(char* commande);
void Executer(char* commande);
char* RecupererCommande();
void cd(char* buffer);
void AnalyseCommande(char* buffer);
void cat_fd(int fd);
void cat(const char *fname);
int countPipe(char* cmd);
bool isSetEnv(char* cmd);
void modiferEnv(char* varEnv, char* nouvelleValeur);
void find(char *nomFichier);

#endif
