#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/wait.h>
#include "signal.h"
#include <pwd.h>
#include "fonction.h"
#include <stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "path.h"

char* Path()
{
  char buffer[256];
  char* path;
  int taillePath = 0;

  getcwd(buffer, sizeof(buffer));

  taillePath = strlen(buffer);

  path = (char*)malloc(taillePath * sizeof(char));

  /* memset(path, 0, sizeof(path)); */
  memset(path, 0, sizeof(taillePath));
  strcat(path, buffer);

  return path;
}

char* RepertoireCourant(){
  //char path[256];
  char *path;
  char hostname[256];
  char* username;
  char* repertoireCourant;
  struct passwd *pwd;
  int tailleRepertoire = 0;

  //getcwd(path, sizeof(path));

  pwd = getpwuid(getuid());
  path = Path();
  username = pwd->pw_name;
  gethostname(hostname, 20);

  tailleRepertoire = strlen(username) + strlen(hostname) + strlen(path) + 5;

  repertoireCourant = (char*)malloc(tailleRepertoire * sizeof(char));

  memset(repertoireCourant, 0, sizeof(tailleRepertoire));
  strcat(repertoireCourant, username);
  strcat(repertoireCourant, "@");
  strcat(repertoireCourant, hostname);
  strcat(repertoireCourant, ":");
  strcat(repertoireCourant, path);
  strcat(repertoireCourant, "> ");

  return repertoireCourant;
}
